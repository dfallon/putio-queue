# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Put-io Queue ###

* This piece of software will create a queueing system for the put.io torrent platform.

### Current brainstorm ###

#### Stories
##### High Priority
* User wants to:
    * view all transfers
    * view their transfers
    * add a transfer
    * remove a transfer
* Admin wants to:
    * change change status of any transfer

##### Low Priority
* User wants to:
    * Change user account details
* Public wants to:
    * Register

#### Models
* User (included with django, needs extension)
* Transfer
    * chinetti, most of your work will be here.
* StatusCode
    * code (primary)
    * description

#### Views
##### High Priority
* All Transfers
* manage my transfers

##### Low Priority
* manage user account

### Contributors
* Daniel Fallon
* Tim McCollam
* Peter Chinetti
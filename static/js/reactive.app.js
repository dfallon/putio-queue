/**
 * Created by dfallon on 2/1/2015.
 */

var TransferItem = React.createClass({displayName: "TransferItem",
    getInitialState: function(){
        return {data: []};
    },
    componentDidMount: function(){
      $.ajax({
          url: '/JSON/transfer/list',
          dataType: 'json',
          success: function(data) {
              this.setState({data: data['transfer_list']});
          }.bind(this),
          error: function(xhr, status, err){
              console.error(status,err.toString());
          }.bind(this)
      })
    },
    render: function(){
        return(React.createElement("li", {className: "collection-item determinate"}, 
        this.state.data.name, 
            React.createElement("span", {className: "badge"}, this.state.data.source), 
            React.createElement("span", {className: "progress", style: {width : this.state.data.percentDone+"%"}})
        ));
    }
});

jQuery(document).ready(function () {
    React.render(
      React.createElement(TransferItem, null),
        document.getElementById('transfer-list')
    );
});
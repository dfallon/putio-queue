from django.conf.urls import patterns, include, url
from django.contrib import admin
from putio_queue_app import conf
from putio_queue_app import views
import django.contrib.auth

import putio_queue_app.views
'''
modsUrlPatterns = patterns(
    *(['']+[url('%s' % mod, include(mod+'.urls')) for mod in conf.DL_MODS])
)
'''

idActionPatterns = patterns(',',
                            url(r'delete', views.delete),
                            url(r'update', views.update),
                            url(r'^$', views.get),  # this is just a naked 'get'
)
transferUrlPatterns = patterns('',
                               url(r'add', views.add),
                               url(r'list', views.get_list),
                               url(r'(?P<transfer_id>\d)/', include(idActionPatterns)),
                               )

jsonUrlPatterns = patterns('',
                           # url(r'mods/', include(modsUrlPatterns)),
                           url(r'transfer/', include(transferUrlPatterns)),
                           )

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'putio_queue.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^JSON/', include(jsonUrlPatterns)),
    url(r'', putio_queue_app.views.app),
)

"""
This is a base template for what to put in your settings_site_specific.py
copy this file to settings_site_specific.py and follow the directions herewithin to create a correctly formed settings file
"""
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Use the following code to generate a new secret key
# This is the same code that django uses for this task
#
# from django.utils.crypto import get_random_string
#
# chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
# print(get_random_string(50, chars))

SECRET_KEY = ''

# debug settings follow normal django convention
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

#database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

SOUTH_DATABASE_ADAPTERS = {}
# for use on the deployed server
# STATIC_ROOT = "/home/p3t3r/putio-queue/static/"
# STATIC_URL = "/static/"

DEFAULT_FILE_DESTINATION = "/"
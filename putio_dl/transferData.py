__author__ = 'andrei'

import json

def encode(dictionary):
    return json.JSONEncoder(dictionary)

def decode(info):
    return json.JSONDecoder(info)
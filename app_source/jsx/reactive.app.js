/**
 * Created by dfallon on 2/1/2015.
 */

var TransferItem = React.createClass({
    getInitialState: function(){
        return {data: []};
    },
    componentDidMount: function(){
      $.ajax({
          url: '/JSON/transfer/list',
          dataType: 'json',
          success: function(data) {
              this.setState({data: data['transfer_list']});
          }.bind(this),
          error: function(xhr, status, err){
              console.error(status,err.toString());
          }.bind(this)
      })
    },
    render: function(){
        return(<li className="collection-item determinate">
        {this.state.data.name}
            <span className="badge">{this.state.data.source}</span>
            <span className="progress" style={{width : this.state.data.percentDone+"%"}}></span>
        </li>);
    }
});

jQuery(document).ready(function () {
    React.render(
      <TransferItem />,
        document.getElementById('transfer-list')
    );
});
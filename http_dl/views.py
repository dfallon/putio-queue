from django.shortcuts import render
from putio_queue_app.models import Transfer
import json
from pyaria2 import pyaria2
# Create your views here.


def add(request, url = None):

    if url == None:
        url = request.POST.get('url')

    aria = pyaria2.PyAria2()
    gid = aria.addUri([url])

    trans = Transfer(
        name=url
    )
    trans.setInfo('gid', gid)
    trans.setInfo('type', 'http')

    if url == None:
        trans.save()
    else:
        return trans
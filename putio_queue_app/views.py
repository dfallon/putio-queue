from django.shortcuts import render,redirect

# Create your views here.
from django.shortcuts import render_to_response
from putio_queue_app import models, utils
from putio_queue_app.utils import fetch_mod
from django.http import HttpResponse
import json


def home(request):
    if request.user.is_authenticated():
        return redirect('url transfer-list')
    return render_to_response('publichome.html')


@fetch_mod
def add(request):
    return request.mod.add(request)


@fetch_mod
def delete(request, transfer_id):
    return request.mod.delete(request, transfer_id)


@fetch_mod
def update(request, transfer_id):
    return request.mod.update(request, transfer_id)


def get_list(request):  # Not named list so as not to shadow built-in
    output = {'status': 'success', 'transfer_list':
        [utils.transfer_to_json(x) for x in models.Transfer.objects.all()]}
    return HttpResponse(json.dumps(output),
                        content_type="application/json")


def get(request, transfer_id):
    transfer = models.Transfer.objects.get(pk=transfer_id)
    if transfer is None:
        return HttpResponse(json.dumps({'transfer_id': transfer_id, 'status': 'success'}),
                            content_type="application/json")
    else:
        return HttpResponse(json.dumps((utils.transfer_to_json(transfer)).update({'status': 'success'})),
                            content_type="application/json")
    
def app(request):
    return render(request, 'app.html')
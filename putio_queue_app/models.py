from django.db import models
from django.contrib.auth.models import AbstractUser
import json
# Create your models here.

# TODO add fields to user model

class QueueUser(AbstractUser):
    averageSize = models.IntegerField(default=0)
    averageSize_count = models.IntegerField(default=0)

# TODO add fields to Transfer model

class Transfer(models.Model):
    owner = models.ForeignKey(QueueUser)
    name = models.CharField(max_length=255)
    size = models.IntegerField()
    percentDone = models.IntegerField()
    source = models.TextField()
    source_visible = models.BooleanField(default=True)
    status = models.CharField(max_length=64)
    otherInfo = models.TextField()

    #python only
    otherInfo_dict = None
    def getInfo(self,field,default=None):
        if self.otherInfo_dict is not None:
            self.otherInfo_dict = dict(json.JSONDecoder(self.otherInfo))
        if self.otherInfo_dict is not None:
            return self.otherInfo_dict.get(field,default=default)
        else:
            return False

    def setInfo(self,field,val):
        self.otherInfo_dict[field] = val

    #Overriding
    def save(self, *args, **kwargs):
        self.otherInfo = json.JSONEncoder(self.otherInfo_dict)
        super(Transfer, self).save(*args, **kwargs)

from django.http import HttpResponse
from putio_queue_app.models import Transfer

__author__ = 'andrei'
import json
from django.views.decorators.http import require_POST
import importlib
from putio_queue_app import conf

def fetch_mod(view_func):
    def _wrapped_view_func(request, *args, **kwargs):
        if request.POST.get('type') in conf.DL_MODS:
            request.mod = importlib.import_module(request.POST.get('type')+'.views')
            return view_func(request, *args, **kwargs)
        else:
            failure_response = {'status': 400, 'title':'unidentified module'}
            return HttpResponse(json.dumps(failure_response), content_type="application/json", status=400)
    return _wrapped_view_func

def transfer_to_json(transfer):
    out = {
        'owner': transfer.owner_id,
        'name': transfer.name,
        'size': transfer.size,
        'percentDone': transfer.percentDone,
        'source': transfer.source,
        'source_visible': transfer.source_visible,
        'status': transfer.status,
        'otherInfo': transfer.otherInfo,
        }
    transfer.getInfo('')
    if transfer.otherInfo_dict is not None:
        out.update(transfer.otherInfo_dict)
    return out
